FROM ubuntu:18.04

MAINTAINER Grégoire Leroy <gregoire.leroy@3e.eu>

RUN apt-get update && apt-get upgrade -y \
    && apt-get install software-properties-common -y \
    && add-apt-repository -y universe \
    && add-apt-repository -y main

# install all deps
RUN apt-get clean && apt-get update && apt-get upgrade -y \
    && apt-get install -y curl bzip2 sudo locales fonts-liberation \
    gfortran g++ build-essential make gcc gcc-multilib \
    liblapack-dev libatlas-base-dev libblas-dev \
    udunits-bin libudunits2-0 libudunits2-dev \
    python3-dev \
    libhdf5-dev libnetcdf-dev libnetcdff-dev netcdf-bin \
    gdal-bin libgdal20 \
    libgeos-dev libproj-dev libproj12 proj-data \
    libeccodes0 \
    && rm -rf /var/lib/apt/lists/*

# set python3 as default python
RUN cd /usr/local/bin \
    && ln -s /usr/bin/python3 python

# Install pip
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python get-pip.py && \
    pip install -U pip && \
    rm get-pip.py